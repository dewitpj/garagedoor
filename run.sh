#!/usr/bin/env bash

CONFIG_PATH=/data/options.json

MQTT_RELAY_TOPIC=$(jq --raw-output '.mqtt_relay_topic // empty' $CONFIG_PATH)
MQTT_STATE_TOPIC=$(jq --raw-output '.mqtt_state_topic // empty' $CONFIG_PATH)
DOOR_OPEN_FIELD=$(jq --raw-output '.door_open_field // empty' $CONFIG_PATH)
DOOR_CLOSED_FIELD=$(jq --raw-output '.door_closed_field // empty' $CONFIG_PATH)
BEAM_FIELD=$(jq --raw-output '.beam_field // empty' $CONFIG_PATH)
MQTT_HOST=$(jq --raw-output '.mqtt_host // empty' $CONFIG_PATH)
MQTT_USER=$(jq --raw-output '.mqtt_user // empty' $CONFIG_PATH)
MQTT_PASS=$(jq --raw-output '.mqtt_pass // empty' $CONFIG_PATH)

python3 /run.py --mqtt-relay-topic ${MQTT_RELAY_TOPIC} --mqtt-state-topic ${MQTT_STATE_TOPIC} --door-open-field ${DOOR_OPEN_FIELD} --door-closed-field ${DOOR_CLOSED_FIELD} --beam-field ${BEAM_FIELD} --mqtt-host ${MQTT_HOST}
