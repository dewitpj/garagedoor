import paho.mqtt.client as mqtt
import time
from datetime import datetime
import os
import sys
import logging
import argparse
import json

run=True

logger=logging.getLogger ()
logger.setLevel (logging.INFO)

dooropen=False
doorclose=True
doorstate="Unknown"
prev_dooropen=False
prev_doorclose=True
prev_doorstate="Unknown"
beam=False

def on_connect(client, userdata, flags, rc):
    logging.info ("We are connected to "+str (args.mqtt_host))
    client.subscribe (args.mqtt_state_topic)

def on_message(client, userdata, msg):
    payload=str (msg.payload.decode ("utf8"))
    logging.debug ("MSG: "+msg.topic+" "+payload)
    if (1==1):
#    try:
        door1json="""
        {
        "name":"Door Open",
        "unique_id":"garagedoor_door1_id",
        "device_class":"garage_door",
        "state_topic":"garagedoor/door1/state"
        }
        """
        door2json="""
        {
        "name":"Door Closed",
        "unique_id":"garagedoor_door2_id",
        "device_class":"garage_door",
        "state_topic":"garagedoor/door2/state"
        }
        """
        beamjson="""
        {
        "name":"Beam",
        "unique_id":"garagedoor_beam_id",
        "device_class":"occupancy",
        "state_topic":"garagedoor/beam/state"
        }
        """
        buttonjson="""
        {
        "name":"Door Button",
        "unique_id":"garagedoor_button_id",
        "command_topic":"""+"\""+args.mqtt_relay_topic+"\""+""",
        "payload_press":"ON"
        }
        """

        client.publish ("homeassistant/binary_sensor/garagedoor/door1/config",door1json)
        client.publish ("homeassistant/binary_sensor/garagedoor/door2/config",door2json)
        client.publish ("homeassistant/binary_sensor/garagedoor/beam/config",beamjson)
        client.publish ("homeassistant/button/garagedoor/config",buttonjson)

        jsondata=json.loads (payload)

        if (jsondata [args.door_open_field]=="OFF"):
            dooropen=False
        else:
            dooropen=True
        if (jsondata [args.door_closed_field]=="OFF"):
            doorclose=False
        else:
            doorclose=True
        if (jsondata [args.beam_field]=="OFF"):
            beam=False
        else:
            beam=True

        if (dooropen==True and doorclose==False):
            doorstate="Moving"
            client.publish ("garagedoor/state","opening")

        if (dooropen):
            client.publish ("garagedoor/door1/state","ON")
            client.publish ("garagedoor/state","state_open")
            #client.publish ("garagedoor/state","state_closed")
        else:
            client.publish ("garagedoor/door1/state","OFF")
            client.publish ("garagedoor/state","state_closed")
            #client.publish ("garagedoor/state","state_open")

        if (doorclose):
            client.publish ("garagedoor/door2/state","OFF")
        else:
            client.publish ("garagedoor/door2/state","ON")

        if (beam):
            client.publish ("garagedoor/beam/state","ON")
        else:
            client.publish ("garagedoor/beam/state","OFF")

        coverjson="""
        {
        "name":"Garage Door",
        "unique_id":"garagedoor_cover_id",
        "device_class":"garage",
        "state_topic":"garagedoor/state",
        "command_topic":"""+"\""+args.mqtt_relay_topic+"\""+""",
        "payload_open":"ON",
        "payload_close":"ON",
        "payload_stop":"ON"
        }
        """
        client.publish ("homeassistant/cover/garagedoor/config",coverjson)
    else:
#    except:
        logging.info ("Failed")

if __name__=="__main__":
    parser=argparse.ArgumentParser (description='Various options for garagedoor.')
    parser.add_argument ("--mqtt-relay-topic",help="MQTT Relay Topic")
    parser.add_argument ("--mqtt-state-topic",help="MQTT State Topic")
    parser.add_argument ("--door-open-field",help="Door Open Field")
    parser.add_argument ("--door-closed-field",help="Door Closed Field")
    parser.add_argument ("--beam-field",help="Beam Field")
    parser.add_argument ("--mqtt-host",help="MQTT Host",default="192.168.4.202")
    parser.add_argument ("--mqtt-user",help="MQTT Username")
    parser.add_argument ("--mqtt-pass",help="MQTT Password")
    args=parser.parse_args ()

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(args.mqtt_host, 1883, 60)
    old_sec=-1
    old_min=-1

    while run:
            client.loop (0.25)
            curr_time=datetime.now ()
            sec=int (curr_time.strftime ("%S"))
            if (sec!=old_sec):
                old_sec=sec

                if (dooropen):
                    #client.publish ("garagedoor/state","state_closed")
                    client.publish ("garagedoor/state","state_open")
                else:
                    #client.publish ("garagedoor/state","state_open")
                    client.publish ("garagedoor/state","state_closed")

                hour=int (curr_time.strftime ("%H"))
                minu=int (curr_time.strftime ("%M"))
                dow=int (curr_time.strftime ("%w"))
