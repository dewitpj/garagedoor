#ARG BUILD_FROM
#FROM $BUILD_FROM

FROM ubuntu:latest

ENV LANG C.UTF-8

# Install python and the needed libs
RUN apt update && apt dist-upgrade -y
RUN apt install python3 jq -y
RUN apt install python3-paho-mqtt -y

WORKDIR /

# Copy data for add-on
COPY run.sh /
COPY run.py /
RUN chmod a+x /run.sh

CMD [ "/run.sh" ]
